﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class to control cards panel, instantiating cards etc
/// </summary>
public class UiCardsController : MonoBehaviour
{
    public static UiCardsController Get { get; set; }

    public UiCardsController()
    {
        Get = this;
    }

    /// <summary>How many mistakes did player do</summary>
    public GameObject CardPrefab;

    /// <summary> List of generated ui cards instances</summary>
    public List<UiCard> Cards = new List<UiCard>();

    /// <summary>Card selected as first by player</summary>
    internal UiCard FirstCard;

    /// <summary>Grid layout group that controlls how cards are placed</summary>
    public GridLayoutGroup LayoutGroup;

    /// <summary>Can player click another card right now ( he cant if cards are animating back in after incorrect match)</summary>
    public bool CanClick = true;

    // Use this for initialization
    void Start()
    {
        //InstantiateCards();
    }

    /// <summary>
    /// Invoked when player taps on some card, if its second turned card it checks if match is correct.
    /// </summary>
    /// <param name="card"></param>
    public void TurnedCard(UiCard card)
    {
        if (FirstCard == null)
        {
            FirstCard = card;
        }
        else
        {
            CanClick = false;
            if (FirstCard.CardId == card.CardId)
            {
                FirstCard.FadeOut();
                card.FadeOut();
                GameController.Get.MatchedCorrectly();
                CanClick = true;
            }
            else
            {
                FirstCard.HideImage();
                card.HideImage();
                GameController.Get.MistakeMade();
            }
            FirstCard = null;
        }
    }

    /// <summary>
    /// Creates all cards and randomizes their positions
    /// </summary>
    public void InstantiateCards()
    {
        Debug.Log("Skad to idzie " + GameController.Get.CurrentDifficulty); 

        List<Sprite> spritesToShow = SpriteHelper.Get.GetSprites((int)GameController.Get.CurrentDifficulty / 2);

        for (int i = 0; i < (int)GameController.Get.CurrentDifficulty / 2; i++)
        {
            UiCard card = Instantiate(CardPrefab, transform).GetComponent<UiCard>();
            UiCard card2 = Instantiate(CardPrefab, transform).GetComponent<UiCard>();
            card.SpriteToShow = spritesToShow[i];
            card2.SpriteToShow = spritesToShow[i];
            card.CardId = i;
            card2.CardId = i;
            Cards.Add(card);
            Cards.Add(card2);
        }
        RandomizeCards();

        AdjustLayoutGroupToDifficulty();

    }

    /// <summary>
    /// Randomizes cards, selects a random one and sets is as last one. x500
    /// </summary>
    public void RandomizeCards()
    {
        for (int i = 0; i < 500; i++)
        {
            Cards[Random.Range(0, Cards.Count - 1)].transform.SetAsLastSibling();
        }
    }

    /// <summary>
    /// Clears all ui card instances and instantiates new ones 
    /// </summary>
    public void ResetCards()
    {
        foreach(UiCard card in Cards)
        {
            Destroy(card.gameObject);
        }
        Cards.Clear();

        InstantiateCards();
    }

    /// <summary>
    /// Every amount of cards needs other layout group setting so it adjusts layout group to fit to current difficulty
    /// </summary>
    public void AdjustLayoutGroupToDifficulty()
    {
        switch (GameController.Get.CurrentDifficulty)
        {
            case EDifficultyLevel.Easy:
                LayoutGroup.cellSize = new Vector2(280, 280);
                LayoutGroup.spacing = new Vector2(30, 30);
                LayoutGroup.constraint = GridLayoutGroup.Constraint.FixedRowCount;
                LayoutGroup.constraintCount = 3;
                break;
            case EDifficultyLevel.Medium:
                LayoutGroup.cellSize = new Vector2(230, 230);
                LayoutGroup.spacing = new Vector2(10, 10);
                LayoutGroup.constraint = GridLayoutGroup.Constraint.FixedRowCount;
                LayoutGroup.constraintCount = 4;
                break;
            case EDifficultyLevel.Hard:
                LayoutGroup.cellSize = new Vector2(150, 150);
                LayoutGroup.spacing = new Vector2(10, 10);
                LayoutGroup.constraint = GridLayoutGroup.Constraint.FixedRowCount;
                LayoutGroup.constraintCount = 6;
                break;
            default:
                break;
        }
    }
}
