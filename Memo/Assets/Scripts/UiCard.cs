﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class to control single card
/// </summary>
public class UiCard : MonoBehaviour
{
    /// <summary>Card image component</summary>
    public Image MainImage;

    /// <summary>Card button component</summary>
    public Button CardButton;

    /// <summary>Color of card image when its turned back(not showed)</summary>
    public Color TurnedCardColor;

    /// <summary> Sprite assigned to this card</summary>
    public Sprite SpriteToShow;

    /// <summary>Default card sprite, showed when its turned back</summary>
    public Sprite DefaultSprite;

    /// <summary>RectTransform component attached to card</summary>
    internal RectTransform Rect;

    /// <summary>Canvas Group component attached to card</summary>
    internal CanvasGroup CanvasGroup;

    /// <summary>Is card currently animated to show its image</summary>
    private bool isAnimatingIn = false;

    /// <summary>Is card currently animated to turn back</summary>
    private bool isAnimatingOut = false;

    /// <summary>Is card currently animated to fade out alpha after it was matched correctly</summary>
    private bool isFadingOut = false;

    /// <summary>Helper value to avoid assignign sprite multiple times</summary>
    private bool changedSprite = false;

    private float helperTimer = 0;

    /// <summary>How long will animations take</summary>
    public float AnimationsTime;

    public float CardStayTimeWhenIncorect;

    /// <summary>Card Id used to identify and compare with other cards</summary>
    public int CardId;


    // Use this for initialization
    void Start()
    {
        Rect = GetComponent<RectTransform>();
        CardButton = GetComponent<Button>();
        CanvasGroup = GetComponent<CanvasGroup>();
        AnimationsTime = 0.5f;
        CardStayTimeWhenIncorect = 0.55f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAnimatingIn)
        {
            helperTimer += Time.deltaTime;

            float easingValue = EasingFunction.EaseInOutCubic(0, 1, helperTimer / AnimationsTime);
            transform.rotation = Quaternion.Euler(Vector3.LerpUnclamped(Vector3.zero, new Vector3(0, 180, 0), easingValue));

            if (transform.rotation.eulerAngles.y >= 90 && !changedSprite)
            {
                MainImage.sprite = SpriteToShow;
                MainImage.color = Color.white;
                changedSprite = true;
            }

            if (helperTimer >= AnimationsTime)
            {
                isAnimatingIn = false;
                transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                UiCardsController.Get.TurnedCard(this);
            }
        }

        if (isAnimatingOut)
        {
            helperTimer += Time.deltaTime;

            if (helperTimer > 0.55f)
            {
                float easingValue = EasingFunction.EaseInOutCubic(0, 1, (helperTimer - CardStayTimeWhenIncorect) / AnimationsTime);
                transform.rotation = Quaternion.Euler(Vector3.LerpUnclamped(new Vector3(0, 180, 0), new Vector3(0, 360, 0), easingValue));

                if (transform.rotation.eulerAngles.y >= 270 && !changedSprite)
                {
                    MainImage.sprite = DefaultSprite;
                    MainImage.color = TurnedCardColor;
                    changedSprite = true;
                }


                if (helperTimer - CardStayTimeWhenIncorect >= AnimationsTime)
                {
                    isAnimatingOut = false;
                    transform.rotation = Quaternion.Euler(Vector3.zero);
                    UiCardsController.Get.CanClick = true;
                }
            }
        }

        if (isFadingOut)
        {
            helperTimer += Time.deltaTime;
            CanvasGroup.alpha = Mathf.Lerp(1, 0, helperTimer / AnimationsTime);

            if (helperTimer >= AnimationsTime)
                isFadingOut = false;
        }
    }

    /// <summary>
    /// Starts to animate card to show its image
    /// </summary>
    public void ShowImage()
    {
        helperTimer = 0;
        changedSprite = false;
        isAnimatingIn = true;
    }

    /// <summary>
    /// Starts to hide card image
    /// </summary>
    public void HideImage()
    {
        helperTimer = 0;
        changedSprite = false;
        isAnimatingOut = true;
    }

    /// <summary>
    /// Invoked when player taps on card
    /// </summary>
    public void OnClickCard()
    {
        if (UiCardsController.Get.CanClick)
            ShowImage();
    }

    /// <summary>
    /// Starts to fade card out
    /// </summary>
    public void FadeOut()
    {
        helperTimer = 0;
        CardButton.interactable = false;
        isFadingOut = true;
    }
}
