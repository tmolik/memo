﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple audio controller to play 
/// </summary>
public class AudioController : MonoBehaviour
{

    public static AudioController Get { get; private set; }

    public AudioController()
    {
        Get = this;
    }

    /// <summary> Main audiosource </summary>
    public AudioSource AudioSource;

    /// <summary> Clip played when player matches two cards correctly </summary>
    public AudioClip CorrectMatchClip;

    /// <summary> Clip played when player finishes matching all cards </summary>
    public AudioClip WinGameClip;

    /// <summary> Is game muted, if yes, there will be no sounds </summary>
    internal bool IsMuted = false;

    /// <summary>
    /// Plays sound of correct match
    /// </summary>
    public void PlayCorrectSound()
    {
        if (!IsMuted)
            AudioSource.PlayOneShot(CorrectMatchClip);
    }

    /// <summary>
    /// Plays sound of winning the game
    /// </summary>
    public void PlayWinSound()
    {
        if (!IsMuted)
            AudioSource.PlayOneShot(WinGameClip);
    }


}
