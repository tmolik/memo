﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple helper to store sprites used in game and to get list of random sprites
/// </summary>
public class SpriteHelper : MonoBehaviour {

    public static SpriteHelper Get { get; private set; }

    public SpriteHelper()
    {
        Get = this;
    }

    /// <summary>All possible sprites</summary>
    public List<Sprite> Sprites = new List<Sprite>();

    /// <summary>
    /// Returns list of random sprites of given amount or null if there is not enough sprites
    /// </summary>
    /// <param name="amount"></param>
    /// <returns></returns>
    public List<Sprite> GetSprites(int amount)
    {
        if (amount > Sprites.Count)
            return null;

        List<Sprite> temporarySpritesList = new List<Sprite>(Sprites);

        List<Sprite> sprites = new List<Sprite>();

        for(int i=0; i<amount; i++)
        {
            Sprite randomSprite = temporarySpritesList[Random.Range(0, temporarySpritesList.Count - 1)];
            temporarySpritesList.Remove(randomSprite);
            sprites.Add(randomSprite);
        }

        return sprites;
    }
}
