﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class to control ui of panel at top of the screen, with mistakes count and buttons
/// </summary>
public class UiTopPanelController : MonoBehaviour
{
    public static UiTopPanelController Get { get; private set; }

    public UiTopPanelController()
    {
        Get = this;
    }

    public Text MistakesText;

    /// <summary> Parent rect of restart buttons</summary>
    public RectTransform RestartButtonsRect;

    /// <summary> Canvas group attached to parent rect of restart buttons</summary>
    public CanvasGroup RestartButtonsCanvasGroup;

    /// <summary>Sprite to show on volume button when game is muted</summary>
    public Sprite MuteOnSprite;

    /// <summary>Sprite to show on volume button when game is not muted</summary>
    public Sprite MuteOffSprite;

    /// <summary>Volume image</summary>
    public Image VolumeOnOffImage;

    /// <summary> Is restart buttons rect animating in</summary>
    private bool isAnimatingRestartButtonsPanelIn;

    /// <summary> Is restart buttons rect actually being showed</summary>
    private bool isRestartPanelShowed;

    /// <summary> Is restart buttons rect animating out right now</summary>
    private bool isHidingRestartButtonsPanel;

    private float helperTimer;

    public void Update()
    {
        if (isAnimatingRestartButtonsPanelIn)
        {
            helperTimer += Time.deltaTime;

            float easingValue = EasingFunction.EaseInOutCubic(0, 1, helperTimer / 0.25f);
            RestartButtonsRect.anchoredPosition = Vector2.LerpUnclamped(new Vector2(0, RestartButtonsRect.sizeDelta.y), new Vector2(0, 0), easingValue);
            RestartButtonsCanvasGroup.alpha = Mathf.Lerp(0, 1, easingValue);

            if (helperTimer >= 0.25f)
            {
                isAnimatingRestartButtonsPanelIn = false;
            }
        }


        if (isHidingRestartButtonsPanel)
        {
            helperTimer += Time.deltaTime;

            float easingValue = EasingFunction.EaseInOutCubic(0, 1, helperTimer / 0.25f);
            RestartButtonsRect.anchoredPosition = Vector2.LerpUnclamped(new Vector2(0, 0), new Vector2(0, RestartButtonsRect.sizeDelta.y), easingValue);
            RestartButtonsCanvasGroup.alpha = Mathf.Lerp(1, 0, easingValue);

            if (helperTimer >= 0.25f)
            {
                isHidingRestartButtonsPanel = false;


            }
        }
    }

    /// <summary>
    /// Clicked mute button, change audio controller value and update volume image sprite
    /// </summary>
    public void OnClickMuteButton()
    {
        AudioController.Get.IsMuted = !AudioController.Get.IsMuted;
        VolumeOnOffImage.sprite = AudioController.Get.IsMuted ? MuteOnSprite : MuteOffSprite;
    }

    /// <summary>
    /// Clicked restart, determines if it should show or hide restart buttons panel and sets values to do that
    /// </summary>
    public void OnClickRestart()
    {
        helperTimer = 0;
        if (isRestartPanelShowed)
        {
            isAnimatingRestartButtonsPanelIn = false;
            isHidingRestartButtonsPanel = true;
            isRestartPanelShowed = false;
        }
        else
        {
            isRestartPanelShowed = true;          
            isAnimatingRestartButtonsPanelIn = true;
            isHidingRestartButtonsPanel = false;
        }

    }

    public void OnClickRestartEasy()
    {
        GameController.Get.CurrentDifficulty = EDifficultyLevel.Easy;
        FinishRestarting();
    }

    public void OnClickRestartMedium()
    {
        GameController.Get.CurrentDifficulty = EDifficultyLevel.Medium;
        FinishRestarting();
    }

    public void OnClickRestartHard()
    {
        GameController.Get.CurrentDifficulty = EDifficultyLevel.Hard;
        FinishRestarting();
    }

    /// <summary>
    /// Function shared for restarting for all difficulties, resets values in this class and tells GameController to restart game
    /// </summary>
    public void FinishRestarting()
    {
        helperTimer = 0;
        isAnimatingRestartButtonsPanelIn = false;
        isHidingRestartButtonsPanel = true;
        isRestartPanelShowed = false;
        GameController.Get.RestartGame();
    }

    /// <summary>
    /// Updates mistakes text with new value
    /// </summary>
    /// <param name="mistakesAmount"></param>
    public void MistakesAmountChanged(int mistakesAmount)
    {
        MistakesText.text = "Mistakes : " + mistakesAmount;
    }
}
