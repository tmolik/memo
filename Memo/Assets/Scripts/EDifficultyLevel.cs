﻿/// <summary>
/// Enum representing game difficulty, int values indicates how many cards will there be
/// </summary>
public enum EDifficultyLevel
{
    Easy = 18,
    Medium = 28,
    Hard= 54
}
