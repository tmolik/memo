﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Get { get; set; }

    public GameController()
    {
        Get = this;
    }

    /// <summary>How many mistakes did player do</summary>
    public int MistakesCount = 0;

    /// <summary>How many cards player matched in this game </summary>
    public int CardsMatched = 0;

    /// <summary>Current difficulty of game</summary>
    public EDifficultyLevel CurrentDifficulty = EDifficultyLevel.Easy;

    public void Start()
    {
        StartCoroutine(StartGameAfterFrame());
    }

    private IEnumerator StartGameAfterFrame()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        GameController.Get.CurrentDifficulty = EDifficultyLevel.Easy;
        RestartGame();
    }

    /// <summary>
    /// Invoked when player makes a mistake, increases mistakes counter and updates ui
    /// </summary>
    public void MistakeMade()
    {
        MistakesCount++;
        UiTopPanelController.Get.MistakesAmountChanged(MistakesCount);
    }

    /// <summary>
    /// Invoked when player matches two cards correctly, plays sound and checks if it's the end of the game
    /// </summary>
    public void MatchedCorrectly()
    {
        CardsMatched += 2;
        if (CardsMatched == (int)CurrentDifficulty)
        {
            AudioController.Get.PlayWinSound();
            UiTopPanelController.Get.OnClickRestart();
        }
        else
            AudioController.Get.PlayCorrectSound();
    }

    /// <summary>
    /// Restarts game, generates new cards and resets counters
    /// </summary>
    public void RestartGame()
    {
        MistakesCount = 0;
        CardsMatched = 0;
        UiTopPanelController.Get.MistakesAmountChanged(0);
        UiCardsController.Get.ResetCards();
    }

}
